echo "Content-type: text/html\r\n"
echo "<TITLE>System Info</TITLE>"
if [ -f "/etc/lsb-release" ]
then
cat /etc/lsb-release | sed "s|^|<br><b>|g" | sed "s|$|</br>|g" | sed "s|=|</b>=|"
elif [ -f "/etc/os-release" ]
then
cat /etc/os-release | sed "s|^|<br><b>|g" | sed "s|$|</br>|g" | sed "s|=|</b>=|"
fi
echo "</p><br><b>Kernel:</b>$(uname -a)</br>"
echo "<br><b>Uptime:</b>$(uptime)</br>"
echo "<p><b>RAM:</b>$(free -h -w)</br>" | sed "s|^|<br><b>|g" | sed "s|$|</br>|g" | sed "s|:|</b>:|"
echo "</p>"
