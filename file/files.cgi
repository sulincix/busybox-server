cd  ../directory
if [ -d "./$QUERY_STRING" ]
then
QUERY_STRING=$(echo $QUERY_STRING | sed "s|\.\.|.|g" | sed "s|//|/|" )
echo "Content-type: text/html;charset=utf-8\r\n"
echo "<html>"
echo "<head><title>$QUERY_STRING</title><head>"
echo "<body bgcolor=\"white\">"
echo "<h1>Index of $QUERY_STRING</h1><hr><TABLE>"
ls -l  $(pwd)/$QUERY_STRING  | awk '{n=split($0,array," "); a="";b="";for(i=9;i<=n;i=i+1){a=a$i" "};for(i=1;i<=8;i=i+1){b=b"<TD>"$i"</TD>"};print "<TR><TD><li><a href=\""a"\">"a"</a></TD><TD>&nbsp;"b"</TD></TR>"}' | grep ":" | sed  "s|=\"|=\"files.cgi?$QUERY_STRING/|" 
else
echo "Content-type: text/html\r\n"
echo "<meta http-equiv=\"refresh\" content=\"0;URL=/directory$QUERY_STRING\">"
fi
echo "</TABLE><hr>"
echo "Busybox http server on <b>$(uname -rmo)</b>"
echo "<html>"
