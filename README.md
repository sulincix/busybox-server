#Kullanım:

Dosyaların olduğu dizinde şunu çalıştırın

busybox httpd -p 80 -fv -h . -c conf

files.cgi anayasfası "directory" dizinidir dizin yoksa oluşturun veya sembolik bağ kurun


#conf dosyası

neyin cgi olarak çalışacağını belirtir. 

yardım için https://wiki.openwrt.org/doc/howto/http.httpd#configuration

#php desteği için:
php-cgi kurun veya derleyin

conf dosyasına şunu ekleyin:
*.php:/usr/bin/php-gci

php.ini dosyasına şunları ekleyin: 
cgi.force_redirect = 0
cgi.redirect_status_env ="yes";

#python desteği için:
conf içine şunu yazın:
*.py /usr/bin/python


